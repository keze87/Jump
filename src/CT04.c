/*
 * César Ezequiel Herrera 97429
 * Numero de grupo: 4
 *
 * Make:
 * gcc -o CT04 CT04.c -lm -std=c11 -Wall -pedantic -pedantic-errors -I.
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include <stdbool.h>

#define GRAVEDAD -9.8
#define ANCHOCELDA 8
#define NCOLUMNAS 4
#define TIEMPOMAX 60

void imprimirEnunciado (char enunciado) {

	switch (enunciado) {
		case 1:
			printf ("\n1) Indicar el número de padrón a utilizar y calcular los datos particulares del grupo.\n\n");
			break;
		case 2:
			printf ("\n2) Expresar la fuerza elástica como función de la coordenada vertical “y”\n");
			printf ("   (tener en cuenta que la cuerda no tiene resistencia a la compresión).\n");
			printf ("   Plantear la ecuación de movimiento correspondiente.\n\n");
			break;
		case 3:
			printf ("\n3) Para el caso particular k2=1, encontrar el punto más bajo de la trayectoria por\n");
			printf ("   consideraciones energéticas o por algún método conocido de otras materias.\n\n");
			break;
		case 4:
			printf ("\n4) Resolver el problema del ítem 3) por el Método de Euler.\n");
			printf ("   Encontrar el paso h necesario para que el error en el punto más bajo sea del 0.1%%.\n");
			printf ("   Comprobar experimentalmente el orden del método.\n\n");
			break;
		case 5:
			printf ("\n5) Resolver el problema del ítem 3) por el Método de Runge Kutta de orden 4.\n");
			printf ("   Encontrar el paso h necesario para que el que error en el punto más bajo sea del 0.1%%.\n");
			printf ("   Comprobar experimentalmente el orden del método.\n\n");
			break;
		case 7:
			printf ("\n7) Dimensionar los parámetros de la cuerda k1 y k2 para que el recorrido en la primera caída supere el 90%%\n");
			printf ("   de H (y no supere el 100%%), y a la vez, la aceleración en el punto más bajo no supere las 2.5g (24.52m/s2).\n");
			printf ("   Aparte, para evitar cualquier brusquedad en el momento que se tensa la cuerda, el valor de k2 deberá ser mayor a 1, indefectiblemente.\n\n");
			break;
		case 8:
			printf ("\n8) Resolver nuevamente el ítem 7 pero incluyendo el efecto de la resistencia del aire\n\n");
			break;
	}

}

/*
 * Manejo de lista
 * https://github.com/fiuba-7541/elemental ?
 */

struct TElementoLista {

	float  t;	// Tiempo
	double y;	// Posicion
	double v;	// Velocidad
	double a;	// Aceleracion

};

/*
 * Movimientos que va a manejar la estructura. Son de conocimiento público,
 * pero sólo deberían usarse para el manejo puntual de esta estructura.
 */
typedef enum TMovimiento_Ls {

	L_Primero,
	L_Siguiente,
	L_Anterior

} TMovimiento_Ls;

/*
 * Estructura auxiliar de la lista simple. Es privada y no debe usarse bajo
 * ningún concepto en la aplicación.
 */
typedef struct TNodoListaSimple {

	void * Elem;
	struct TNodoListaSimple * Siguiente, * Anterior;

} TNodoListaSimple;

/*
 * Estructura cabecera, este es el tipo que se deberá instanciar, aunque
 * nunca acceder a sus campos.
 */
typedef struct TListaSimple {

	TNodoListaSimple * Primero, * Corriente;
	size_t TamanioDato;

} TListaSimple;

/*
 * L_CREAR
 * Pre: Ls no fue creada.
 * Post: Ls creada y vacia
 */
void L_Crear (TListaSimple * pLs, size_t TamanioDato) {

	pLs->Corriente = NULL;
	pLs->Primero = NULL;
	pLs->TamanioDato = TamanioDato;

}

/*
 * L_VACIAR
 * Pre: Ls creada.
 * Post: Ls vacia.
 */
void L_Vaciar (TListaSimple * pLs) {

	TNodoListaSimple * pNodo, * Siguiente;

	for (pNodo = pLs->Primero; pNodo; pNodo = Siguiente) {
		Siguiente = pNodo->Siguiente;
		free (pNodo->Elem);
		free (pNodo);
	}

	pLs->Primero = pLs->Corriente = NULL;

}

/*
 * L_VACIA
 * Pre: Ls creada.
 * Post: Si Ls tiene elementos devuelve FALSE sino TRUE.
 */
bool L_Vacia (TListaSimple Ls) {

	if (Ls.Primero == NULL)
		return true;

	return false;

}

/*
 * L_ELEM_CTE
 * Pre: Ls creada y no vacia.
 * Post: Se devuelve en E el elemento Corriente de la lista.
 */
void L_Elem_Cte (TListaSimple Ls, void * pE) {

	memcpy (pE, Ls.Corriente->Elem, Ls.TamanioDato);

}

/*
 * L_MOVER_CTE
 * Pre: Ls creada
 * Post: Si Ls esta vacia, devuelve FALSE. Sino:
 * Si M = L_Primero, el nuevo elemento Corriente es el Primero. Devuelve TRUE
 * Si M = L_Siguiente, el nuevo elemento Corriente es el Siguiente al
 * Corriente. Si estaba en el ultimo elemento, devuelve FALSE, sino TRUE.
 * Si M = L_Anterior, el nuevo elemento Corriente es el Anterior al
 * Corriente. Si estaba en el primer elemento, devuelve FALSE, sino TRUE.
 */
bool L_Mover_Cte (TListaSimple * pLs, TMovimiento_Ls M) {

	if (L_Vacia (* pLs))
		return false;

	switch (M) {
		case L_Primero:
			pLs->Corriente = pLs->Primero;
			break;
		case L_Siguiente:
			if (pLs->Corriente->Siguiente == NULL)
				return false;
			else
				pLs->Corriente = pLs->Corriente->Siguiente;
			break;
		case L_Anterior:
			if (pLs->Corriente->Anterior == NULL)
				return false;
			else
				pLs->Corriente = pLs->Corriente->Anterior;
			break;
	}

	return true;

}

/*
 * L_INSERTAR_CTE
 * Pre: Ls creada.
 * Post: E se agrego a la lista y es el actual elemento Corriente.
 * Si M=L_Primero: se inserto como Primero de la lista.
 * Si M=L_Siguiente: se inserto despues del elemento Corriente.
 * Si M=L_Anterior: se inserto antes del elemento Corriente.
 * Si pudo insertar el elemento devuelve TRUE, sino FALSE.
 */
bool L_Insertar_Cte (TListaSimple * pLs, TMovimiento_Ls M, void * pE) {

	TNodoListaSimple * pNodo = malloc (sizeof (TNodoListaSimple));

	if (!pNodo)
		return false; // No hay memoria disponible

	pNodo->Elem = malloc (pLs->TamanioDato);

	if (!pNodo->Elem) {
		free (pNodo);
		return false;
	}

	memcpy (pNodo->Elem, pE, pLs->TamanioDato);

	if ((pLs->Primero == NULL) || (M == L_Primero) || ((M == L_Anterior) && (pLs->Primero == pLs->Corriente))) {
		/*Si está vacía o hay que insertar en el Primero o
		hay que insertar en el Anterior y el actual es el Primero */
		pNodo->Siguiente = pLs->Primero;
		if (pLs->Primero != NULL)
			pLs->Primero->Anterior = pNodo;
		pLs->Primero = pLs->Corriente = pNodo;
	} else {
		// Siempre inserto como siguiente, el nodo nuevo, porque es más fácil
		pNodo->Siguiente = pLs->Corriente->Siguiente;
		pNodo->Anterior = pLs->Corriente;
		if (pLs->Corriente->Siguiente != NULL)
			pLs->Corriente->Siguiente->Anterior = pNodo;
		pLs->Corriente->Siguiente = pNodo;

		if (M == L_Anterior) {
			// Pero cuando el movimiento es Anterior, entonces swapeo los elementos
			void * tmp = pNodo->Elem;
			pNodo->Elem = pLs->Corriente->Elem;
			pLs->Corriente->Elem = tmp;
		}
	}

	pLs->Corriente = pNodo;
	return true;

}

// Creo lista con elemento inicial T(0) = t0
TListaSimple crearListaVI (double valorInicial) {

	TListaSimple lista;
	L_Crear (& lista, sizeof (struct TElementoLista));

	struct TElementoLista vInicial;
	vInicial.t = 0;
	vInicial.a = GRAVEDAD;
	vInicial.v = 0;
	vInicial.y = valorInicial;
	L_Insertar_Cte (& lista, L_Primero, & vInicial);

	return lista;

}

// Imprime línea entre filas
void imprimirLineaSeparadora (bool ultimo) {

	printf ("\n");

	if (ultimo == false)
		printf("├");
	else
		printf("└");

	// Ancho de cada celda * tamaño de fila + separadores
	for (size_t i = 0; i < NCOLUMNAS; i++) {
		for (size_t j = 0; j <= ANCHOCELDA; j++)
			printf ("─");

		if (i != NCOLUMNAS - 1) {
			if (ultimo == false)
				printf("┼─");
			else
				printf("┴─");
		}
	}

	if (ultimo == false) {
		printf("┤\n");
		printf("│");
	} else
		printf("┘\n");

}

// Imprime espacios y un separador
void imprimirStringConSeparador (char * string, short flecha) {

	size_t largoString = strlen (string);

	if (flecha != 0) {
		largoString++;

		if (flecha > 0)
			printf("↑");
		else
			printf("↓");
	}

	while (largoString < ANCHOCELDA) {
		printf (" ");
		largoString++;
	}

	printf ("%s │ ", string);

}

// Imprime lista a pantalla
void imprimirLista (TListaSimple lista, float h) {

	size_t i = round (1 / (h * 3));
	bool puedoRecorrerLista = L_Mover_Cte (& lista, L_Primero);

	printf ("┌─────────┬──────────┬──────────┬──────────┐\n");
	printf ("│   t (s) │    a (g) │ v (km/h) │    y (m) │");

	while (puedoRecorrerLista) {
		struct TElementoLista elem;
		L_Elem_Cte (lista, & elem);

		if (i == round (1 / (h * 3))) { // No imprimo todos los elementos
			// Pido memoria
			char * tiempo = 		malloc (sizeof (char) * ANCHOCELDA);
			char * aceleracion = 	malloc (sizeof (char) * ANCHOCELDA);
			char * velocidad = 		malloc (sizeof (char) * ANCHOCELDA);
			char * posicion = 		malloc (sizeof (char) * ANCHOCELDA);

			// Convierto de float a string
			snprintf (tiempo, ANCHOCELDA, "%.2f", elem.t);
			snprintf (posicion, ANCHOCELDA, "%.2f", elem.y);
			snprintf (aceleracion, ANCHOCELDA, "%.2f", fabs (elem.a / GRAVEDAD));
			snprintf (velocidad, ANCHOCELDA, "%.2f", fabs (elem.v) * 3.6); // km/h

			// Imprimo
			imprimirLineaSeparadora (false);
			imprimirStringConSeparador (tiempo, 0);

			// Agrego direccion del vector
			if (elem.a > 0)
				imprimirStringConSeparador (aceleracion, 1);
			else if (elem.a < 0)
				imprimirStringConSeparador (aceleracion, -1);
			else
				imprimirStringConSeparador (aceleracion, 0);

			if (elem.v > 0)
				imprimirStringConSeparador (velocidad, 1);
			else if (elem.v < 0)
				imprimirStringConSeparador (velocidad, -1);
			else
				imprimirStringConSeparador (velocidad, 0);

			imprimirStringConSeparador (posicion, 0);

			free (tiempo); free (aceleracion); free (velocidad); free (posicion);
			i = 0;
		}

		puedoRecorrerLista = L_Mover_Cte (& lista, L_Siguiente);
		i++;
	}

	imprimirLineaSeparadora (true); // Ultima linea

}

/*
 * Datos
 */

struct TVectorDatos {

	size_t numeroDePadron;
	size_t altura;
	float longitudCuerda;
	float masa;
	float k1;
	float k2;
	float c1;
	float c2;

};

struct TVectorDatos cargarVectorDatos () {

	struct TVectorDatos retorno;

	retorno.numeroDePadron = 97429;
	retorno.altura = 150;
	retorno.longitudCuerda = retorno.altura * (0.25 + 0.1 * (retorno.numeroDePadron - 90000) / 10000);
	retorno.masa = 50 + 40 * (double) (retorno.numeroDePadron - 90000) / 10000;
	retorno.k1   = 40 + 10 * (double) (retorno.numeroDePadron - 90000) / 10000;
	retorno.k2   =  1;
	retorno.c1   =  3 +  2 * (double) (retorno.numeroDePadron - 90000) / 10000;
	retorno.c2   =  1.5;

	printf ("NP = %zu\n", retorno.numeroDePadron);
	printf (" H = %zu\n", retorno.altura);
	printf ("L0 = %zu*(0.25+0.1*(%zu-90000)/10000) = %f\n", retorno.altura, retorno.numeroDePadron, retorno.longitudCuerda);
	printf (" m = 50+40*(%zu-90000)/10000 = %f\n", retorno.numeroDePadron, retorno.masa);
	printf ("k1 = 40+10*(%zu-90000)/10000 = %f\n", retorno.numeroDePadron, retorno.k1);
	printf ("k2 =  1\n");
	printf ("c1 =  3+ 2*(%zu-90000)/10000 =  %f\n", retorno.numeroDePadron, retorno.c1);
	printf ("c2 =  1.5\n");
	printf (" g = %.1f\n", GRAVEDAD);

	return retorno;

}

/*
 * Métodos
 */

typedef enum EMetodos {

	Euler,
	RK4

} EMetodos;

// PRE: Lista no vacia
void euler (double (* funcion)(double, double, struct TVectorDatos), TListaSimple * lista, float h, struct TVectorDatos datos) {

	struct TElementoLista n;
	L_Elem_Cte (* lista, & n);

	short numeroCaidas = 0;
	while ((numeroCaidas < 4) && (n.t < TIEMPOMAX)) {
		struct TElementoLista n1;
		n1.t = n.t + h;
		n1.v = n.v + h * n.a;
		n1.y = n.y + h * n.v;
		n1.a = funcion (n.y, n.v, datos);

		L_Insertar_Cte (lista, L_Siguiente, & n1);

		if ((n1.a > 0) && (n1.v > 0) && (n1.v > 0) && (n.v <= 0)) // Busco el instante donde empieza a subir
			numeroCaidas++;

		n.t = n1.t;
		n.v = n1.v;
		n.y = n1.y;
		n.a = n1.a;
	}

}

// PRE: Lista no vacia
void rungeKutta (double (* funcion)(double, double, struct TVectorDatos), TListaSimple * lista, float h, struct TVectorDatos datos) {

	struct TElementoLista n;
	L_Elem_Cte (* lista, & n);

	short numeroCaidas = 0;
	while ((numeroCaidas < 4) && (n.t < TIEMPOMAX)) {
		double l1, l2, l3, l4;
		double k1, k2, k3, k4;
		struct TElementoLista n1;
		l1 = h * funcion (n.y, n.v, datos);							k1 = h * n.v;
		l2 = h * funcion (n.y + 0.5 * k1, n.v + 0.5 * l1, datos);	k2 = h * (n.v + 0.5 * l1);
		l3 = h * funcion (n.y + 0.5 * k2, n.v + 0.5 * l2, datos);	k3 = h * (n.v + 0.5 * l2);
		l4 = h * funcion (n.y + k3, n.v + l3, datos);				k4 = h * (n.v + l3);

		n1.t = n.t + h;
		n1.v = n.v + (l1 + 2 * l2 + 2 * l3 + l4) / 6;
		n1.y = n.y + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		n1.a = funcion (n.y, n.v, datos);

		L_Insertar_Cte (lista, L_Siguiente, & n1);

		if ((n1.a > 0) && (n1.v > 0) && (n1.v > 0) && (n.v <= 0)) // Busco el instante donde empieza a subir
			numeroCaidas++;

		n.t = n1.t;
		n.v = n1.v;
		n.y = n1.y;
		n.a = n1.a;
	}

}

/*
 * Funciones
 */

double funcionSinRozamiento (double yn, double vn __attribute__((unused)), struct TVectorDatos d /* datos */) {

	double fElastica = 0;
	if (yn < d.altura - d.longitudCuerda) // Si la cuerda está estirada
		fElastica = d.k1 * pow (d.altura - d.longitudCuerda - yn, d.k2);

	return fElastica / d.masa + GRAVEDAD; // = y''

}

double funcionConRozamiento (double yn, double vn, struct TVectorDatos d /* datos */) {

	double peso = d.masa * GRAVEDAD; // < 0

	double fElastica = 0;
	if (yn < d.altura - d.longitudCuerda) // Si la cuerda está estirada
		fElastica = d.k1 * pow (d.altura - d.longitudCuerda - yn, d.k2);

	double fViscosa = d.c1 * pow (fabs (vn), d.c2);
	if (vn > 0) // Se opone al desplazamiento
		fViscosa *= -1;

	return (peso + fElastica + fViscosa) / d.masa; // = y''

}

/*
 * Proceso
 */

void calcularErrorYMin (TListaSimple lista, float h) {

	const double ymin = 41.59842537471833;

	bool puedoRecorrerLista = L_Mover_Cte (& lista, L_Primero);
	struct TElementoLista anterior;
	L_Elem_Cte (lista, & anterior);

	while (puedoRecorrerLista) {
		struct TElementoLista actual;
		L_Elem_Cte (lista, & actual);

		if ((actual.v > 0) && (anterior.v <= 0)) { // Busco el instante donde empieza a subir
			printf ("Con h = %.4f, Error = %.4f%%\n", h, fabs (100 * (ymin - anterior.y) / ymin));
			break;
		}

		anterior = actual;
		puedoRecorrerLista = L_Mover_Cte (& lista, L_Siguiente);
	}

}

void resolverSinRozamiento (struct TVectorDatos datos, float h, EMetodos metodo) {

	TListaSimple lista = crearListaVI (datos.altura);

	switch (metodo) {
		case Euler:
			euler (funcionSinRozamiento, & lista, h, datos);
			break;
		case RK4:
			rungeKutta (funcionSinRozamiento, & lista, h, datos);
			break;
	}

	imprimirLista (lista, h);
	calcularErrorYMin (lista, h);
	L_Vaciar (& lista);

}

bool cumpleCondiciones (TListaSimple lista, struct TVectorDatos datos, float h) {

	bool puedoRecorrerLista = L_Mover_Cte (& lista, L_Primero);
	struct TElementoLista anterior;
	L_Elem_Cte (lista, & anterior);

	while (puedoRecorrerLista) {
		struct TElementoLista actual;
		L_Elem_Cte (lista, & actual);

		if ((actual.v > 0) && (anterior.v <= 0)) { // Busco el instante donde empieza a subir
			if ((anterior.y < 0.1 * datos.altura) && (anterior.y > 0) \
			&& (fabs (anterior.a) < fabs (2.5 * GRAVEDAD))) {
				imprimirLista (lista, h);
				printf ("ymin = %.2f, |a| = %.2f\n", anterior.y, fabs (anterior.a / GRAVEDAD));
				return true;
			}

			return false;
		}

		anterior = actual;
		puedoRecorrerLista = L_Mover_Cte (& lista, L_Siguiente);
	}

	return false;
}

bool buscarK1K2 (struct TVectorDatos datos, double (* funcion)(double, double, struct TVectorDatos)) {

	const float h = 0.001;

	for (short k1 = 1; k1 <= 100; k1 += 1) {
		for (float k2 = 1.1; k2 <= 2; k2 += .1) {
			datos.k1 = k1;
			datos.k2 = k2;

			TListaSimple lista = crearListaVI (datos.altura);
			rungeKutta (funcion, & lista, h, datos);

			if (cumpleCondiciones (lista, datos, h)) {
				printf ("Con k1 = %d, k2 = %.2f.\n", k1, k2);
				L_Vaciar (& lista);

				return true;
			}

			L_Vaciar (& lista);
		}
	}

	return false;

}

bool proceso () {

	struct TVectorDatos datos;

	imprimirEnunciado (1);
	datos = cargarVectorDatos ();

	imprimirEnunciado (2);
	printf ("m*a = ΣF\n");
	printf ("a = y''\n");
	printf ("m*y'' = Felastica-m*g\n\n");
	printf ("La persona salta desde y=150 hasta y=ymin > 0.\n");
	printf ("Felastica = {  0   si y ≥ H-L0\n");
	printf ("              k1*x si y < H-L0 }\n");
	printf ("x = H-L0-y\n");

	imprimirEnunciado (3);
	printf ("m*g*H = 0.5*k1*x^2+m*g*ymin\n");
	printf ("x = H-L0-ymin\n");
	printf ("∴ ymin ≈ 41.5984253747183\n");

	imprimirEnunciado (4);
	resolverSinRozamiento (datos, 0.0001, Euler);

	imprimirEnunciado (5);
	resolverSinRozamiento (datos, 0.01, RK4);

	imprimirEnunciado (7);
	buscarK1K2 (datos, funcionSinRozamiento);

	imprimirEnunciado (8);
	buscarK1K2 (datos, funcionConRozamiento);

	return EXIT_SUCCESS;

}

int main () {

	printf ("\nCesar Ezequiel Herrera 97429\n");

	return proceso ();

}
